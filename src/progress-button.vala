[GtkTemplate (ui = "/org/example/App/progress-button.ui")]
public class ProgressButton.ProgressButton : Gtk.Bin {
    public signal void clicked ();

    public string label { get; set; }
    public float progress { get; set; }
    public string? action_name { get; set; }
    public Variant? action_target { get; set; }

    static construct {
        set_css_name ("progressbutton");

        typeof (ClipBin).ensure ();
    }

    [GtkCallback]
    private void clicked_cb () {
        clicked ();
    }
}
