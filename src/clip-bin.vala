public class ProgressButton.ClipBin : Gtk.Bin {
    static construct {
        set_css_name ("clipbin");
    }

    protected override bool draw (Cairo.Context cr) {
        var context = get_style_context ();
        var state = get_state_flags ();
        int width = get_allocated_width ();
        int height = get_allocated_height ();

        int border_radius = (int) context.get_property (Gtk.STYLE_PROPERTY_BORDER_RADIUS, state);
        border_radius = border_radius.clamp (0, int.min (width / 2, height / 2));

        rounded_rectangle (cr, 0, 0, width, height, border_radius);
        cr.clip ();

        base.draw (cr);

        return Gdk.EVENT_PROPAGATE;
    }

    private void rounded_rectangle (Cairo.Context cr, double x, double y, double width, double height, double radius) {
        const double ARC_0 = 0;
        const double ARC_1 = Math.PI * 0.5;
        const double ARC_2 = Math.PI;
        const double ARC_3 = Math.PI * 1.5;

        cr.new_sub_path ();
        cr.arc (x + width - radius, y + radius,          radius, ARC_3, ARC_0);
        cr.arc (x + width - radius, y + height - radius, radius, ARC_0, ARC_1);
        cr.arc (x + radius,         y + height - radius, radius, ARC_1, ARC_2);
        cr.arc (x + radius,         y + radius,          radius, ARC_2, ARC_3);
        cr.close_path ();
    }
}
