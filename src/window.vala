[GtkTemplate (ui = "/org/example/App/window.ui")]
public class ProgressButton.Window : Gtk.ApplicationWindow {
    public Window (Gtk.Application app) {
        Object (application: app);
    }

    static construct {
        typeof (ProgressButton).ensure ();
    }
}
